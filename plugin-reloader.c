/*
 * claws-mail-plugin-reloader -- Quick plugin reloader plugin for Claws Mail
 * Copyright (C) 2015 Charles Lehner and the Claws Mail Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>

#define GETTEXT_PACKAGE "claws-mail-plugin-reloader"
#include <glib/gi18n-lib.h>

#include <gtk/gtk.h>

#include "plugin.h"
#include "mainwindow.h"
#include "menu.h"
#include "alertpanel.h"

#define PLUGIN_NAME (_("Plugin Reloader"))
#define VERSION "1.0.0"

static void update_reload_plugins_menu(void);

GtkWidget *plugins_menuitem;
MainWindow *mainwin;
static guint main_menu_id = 0;

const gchar *plugin_name(void) { return PLUGIN_NAME; }
const gchar *plugin_version(void) { return VERSION; }
const gchar *plugin_type(void) { return "GTK2"; }
const gchar *plugin_licence(void) { return "GPL3+"; }

struct PluginFeature *plugin_provides(void)
{
	static struct PluginFeature features[] = 
		{ {PLUGIN_UTILITY, N_("Plugin Reloader")},
		  {PLUGIN_NOTHING, NULL}};
	return features;
}

const gchar *plugin_desc(void)
{
	return _("This plugin lets you quickly reload other plugins. "
		"It is meant as a tool to help speed up plugin development.");
}

static GtkActionEntry reloader_main_menu[] = {
	{"Tools/ReloadPlugin", NULL, N_("Reload Plugin"), NULL, NULL,
		G_CALLBACK(gtk_false)}
};

gint plugin_init(gchar **error)
{
	mainwin = mainwindow_get_mainwindow();

	gtk_action_group_add_actions(mainwin->action_group, reloader_main_menu,
			G_N_ELEMENTS(reloader_main_menu), mainwin);

	MENUITEM_ADDUI_ID_MANAGER(mainwin->ui_manager,
			"/Menu/Tools", "ReloadPlugin", "Tools/ReloadPlugin",
			GTK_UI_MANAGER_MENUITEM, main_menu_id);

	debug_print("Plugin Reloader plug-in loaded\n");

	plugins_menuitem = gtk_ui_manager_get_widget(mainwin->ui_manager,
			"/Menu/Tools/ReloadPlugin");
	g_signal_connect(G_OBJECT(plugins_menuitem), "activate",
			G_CALLBACK(update_reload_plugins_menu), NULL);

	update_reload_plugins_menu();

	return 0;
}

gboolean plugin_done(void)
{
	MainWindow *mainwin = mainwindow_get_mainwindow();

	if (mainwin == NULL)
		return TRUE;

	MENUITEM_REMUI_MANAGER(mainwin->ui_manager, mainwin->action_group,
			"Tools/ReloadPlugin", main_menu_id);
	main_menu_id = 0;

	debug_print("Plugin Reloader plugin unloaded\n");

	return TRUE;
}

static void reload_plugin_cb(GtkAction *action, gpointer data)
{
	Plugin *plugin = data;
	struct {
		gchar *filename;
	} *pluginfoo = data;
	gchar *filename;
	gchar *err = NULL;
	
	Xstrdup_a(filename, pluginfoo->filename, return)

	if (plugin_get_name(plugin) == PLUGIN_NAME) {
		alertpanel_error(_("Can't do that."));
		/* Can't remove self because then we would return into unloaded code.
		 * Can't remove in a timeout/idle because those need to return false */
		return;
	}

	plugin_unload(plugin);
	plugin_load(filename, &err);

	if (err) {
		alertpanel_error(err);
		g_free(err);
	}

	update_reload_plugins_menu();
}

static void update_reload_plugins_menu(void)
{
	GSList *cur;
	GtkWidget *menu = gtk_menu_new();
	GSList *plugins_list = plugin_get_list();
	GtkUIManager *ui_manager = mainwin->ui_manager;
	static const gchar *accel_group = "<ReloadPlugins>/";
	gchar *accel_path;

	gtk_menu_set_accel_group(GTK_MENU(menu),
			gtk_ui_manager_get_accel_group(ui_manager));

	gtk_menu_item_set_submenu(GTK_MENU_ITEM(plugins_menuitem), NULL);

	for (cur = plugins_list; cur != NULL; cur = cur->next) {
		Plugin *plugin = cur->data;
		const gchar *plugin_name = plugin_get_name(plugin);
		GtkWidget *item = gtk_menu_item_new_with_label(plugin_name);

		gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
		g_signal_connect(G_OBJECT(item), "activate",
				G_CALLBACK(reload_plugin_cb), plugin);

		Xstrcat_a(accel_path, accel_group, plugin_name, continue);
		gtk_menu_item_set_accel_path(GTK_MENU_ITEM(item), accel_path);
	}

	gtk_widget_show_all(menu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(plugins_menuitem), menu);

	g_slist_free(plugins_list);
}

